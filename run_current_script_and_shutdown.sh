# Ask for sudo first
[ "$UID" -eq 0 ] || exec sudo bash "$0" "$@"

python3 src/models/train_model.py && sleep 300 && shutdown now

## Notes
The validation accuracy has definitely improved above the baseline.

The maximum accuracy obtained before was `0.8051999986`, at epoch 287/2000. Now it has reached `0.9236000023` at 1924/2000.
Strangely enough, pretty much all high accuracy results are achieved beyond epoch 1500, but the validation loss is the lowest at around epoch 25, and then is scattared arbitrarily, but before reaching epoch 1000.

Does this mean that the variance in the obtained features is high? Or does the training dataset not adequately represent the real data? I'll have to think about this some more.

At any rate, these are the changes I should enact:
-[ ] **Choose which model to save as the best, based on accuracy**. Currently it is based on the loss, but that may not be the best option. There's at least a `2%` difference in accuracy between the first 10 of best validation accuracy results and best validation loss results.
-[ ] **Average out the data before plotting the graphs, as it's a bit too bumpy.** I should have several grapsh being plotted as they don't taku up much space anyway. Also, maybe I should look into some better plotting software or a format that can at least be zoomed in. (Tensorboard, Seaborne, other format of Matplotlib?)
-[ ] **Investigate smaller learning rates**. Now that I can train on a GPU, maybe I could decrease the learning rate or implement annealing (is that the word?).
import os.path
import sys
assert os.path.exists(".git"), "Script needs to be run from a top-level folder in a git repo."
sys.path.append(os.getcwd())

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Dropout
from keras.optimizers import Adam

from src.util.results_recorder import ResultsRecorder


def check_devices(assert_gpu=False, assert_cpu=True):
    """
    Print the list of devices available.
    """
    from tensorflow.python.client import device_lib
    devices = device_lib.list_local_devices()

    assert len(devices) > 0
    assert (not assert_gpu or any([dev.device_type == "GPU" for dev in devices])), "No GPU detected."
    assert (not assert_cpu or any([dev.device_type == "CPU" for dev in devices])), "No CPU detected."


def train_model(run_name):
    ##### Define Model #####
    general_hyperparams = {
        "epochs": 2000,
        "batch_size": 512,
        "padding": "same",
        "activation": "relu",
        "optimizer": {
            "name": "adam",
            "learning_rate": 0.0005,
            "decay": 0.01
        },
        "error": "binary_crossentropy",
        "data_aug": {
            "shear_range": 0.2,
            "zoom_range": 0.2,
            "horizontal_flip": True
        },
        "input_dim": 64,
        "dropout_rate": 0.5
    }

    # Initialising the CNN
    classifier = Sequential()

    # Convolution
    # In: 64
    classifier.add(Conv2D(
        filters=100,
        kernel_size=(3, 3),
        padding=general_hyperparams["padding"],
        activation=general_hyperparams["activation"],
        input_shape=(general_hyperparams["input_dim"], general_hyperparams["input_dim"], 3)
    ))
    classifier.add(MaxPooling2D(strides=(2,2)))

    # Convolution
    # In: 32
    classifier.add(Conv2D(
        filters=200,
        kernel_size=(3, 3),
        padding=general_hyperparams["padding"],
        activation=general_hyperparams["activation"]
    ))
    classifier.add(MaxPooling2D(strides=(2,2)))

    # Convolution
    # In: 16
    classifier.add(Conv2D(
        filters=300,
        kernel_size=(3, 3),
        padding=general_hyperparams["padding"],
        activation=general_hyperparams["activation"]
    ))
    classifier.add(MaxPooling2D(strides=(2,2)))

    # Convolution
    # In: 8
    classifier.add(Conv2D(
        filters=400,
        kernel_size=(3, 3),
        padding=general_hyperparams["padding"],
        activation=general_hyperparams["activation"]
    ))
    classifier.add(MaxPooling2D(strides=(2,2)))

    # Convolution
    # In: 4
    classifier.add(Conv2D(
        filters=500,
        kernel_size=(3, 3),
        padding=general_hyperparams["padding"],
        activation=general_hyperparams["activation"]
    ))
    classifier.add(MaxPooling2D(strides=(2,2)))

    # Convolution
    # In: 2
    classifier.add(Conv2D(
        filters=600,
        kernel_size=(3, 3),
        padding=general_hyperparams["padding"],
        activation=general_hyperparams["activation"]
    ))
    classifier.add(MaxPooling2D(strides=(2,2)))

    classifier.add(Flatten())
    classifier.add(Dropout(general_hyperparams["dropout_rate"]))
    classifier.add(Dense(units=1, activation='sigmoid'))

    # Optimizer
    optimizer = Adam(
        lr=general_hyperparams["optimizer"]["learning_rate"],
        decay=general_hyperparams["optimizer"]["decay"]
    )

    # Compiling the CNN
    classifier.compile(
        optimizer=optimizer,
        loss=general_hyperparams["error"],
        metrics=['accuracy']
    )
    
    ##### Augment Data #####
    train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=general_hyperparams["data_aug"]["shear_range"],
        zoom_range=general_hyperparams["data_aug"]["zoom_range"],
        horizontal_flip=general_hyperparams["data_aug"]["horizontal_flip"])

    valid_datagen = ImageDataGenerator(rescale=1./255)

    data_folder = "data"
    assert os.path.isdir(data_folder), "Data folder with path '%s' not found." % data_folder

    train_data_folder = os.path.join(data_folder, "train")
    assert os.path.isdir(train_data_folder), "Training data folder with path '%s' not found." % train_data_folder
    training_set = train_datagen.flow_from_directory(
        train_data_folder,
        classes=["dogs", "cats"],
        target_size=(general_hyperparams["input_dim"], general_hyperparams["input_dim"]),
        batch_size=general_hyperparams["batch_size"],
        class_mode='binary')

    valid_data_folder = os.path.join(data_folder, "valid")
    assert os.path.isdir(valid_data_folder), "Validation data folder with path '%s' not found." % valid_data_folder
    valid_set = valid_datagen.flow_from_directory(
        valid_data_folder,
        classes=["dogs", "cats"],
        target_size=(general_hyperparams["input_dim"], general_hyperparams["input_dim"]),
        batch_size=general_hyperparams["batch_size"],
        class_mode='binary')

    ##### Train Model #####
    with ResultsRecorder("results", run_name, time_record_filename="runtime.txt") as results_recorder:
        results_recorder.save_summary(classifier)
        results_recorder.plot_simple_model_schema(classifier)
        results_recorder.record_in_json(general_hyperparams, "general_hyperparams.json")

        csv_logger = results_recorder.get_csv_logger()
        save_best_model_callback = results_recorder.save_best_model_callback()

        # Train
        history = classifier.fit_generator(
            training_set,
            steps_per_epoch=19997/general_hyperparams["batch_size"], # number of samples divided by batch size
            epochs=general_hyperparams["epochs"],
            validation_data=valid_set,
            validation_steps=5000/general_hyperparams["batch_size"],
            use_multiprocessing=True,
            workers=16,
            max_queue_size=25,
            callbacks=[csv_logger, save_best_model_callback])

        ##### Plot Results #####
        results_recorder.save_checkpoint(classifier)
        results_recorder.save_keras_error_plot(history)
        results_recorder.save_keras_accuracy_plot(history)

if __name__ == "__main__":
    check_devices(assert_cpu=True, assert_gpu=True)
    train_model(run_name="64x64_deep_cnn_dropout_slower")

import os.path
import sys
assert os.path.exists(".git"), "Script needs to be run from a top-level folder in a git repo."
sys.path.append(os.getcwd())

# Memory Check
from keras.callbacks import Callback
import gc
import resource


class MemoryCallback(Callback):
    """
    Check memory usage of the model.
    """
    def on_epoch_end(self, epoch, log={}):
        if epoch % 10 == 0:
            print("\n[META] Pre-collect memory use:", self._get_mem_use())
            gc.collect()
            print("\n[META] Post-collect memory use:", self._get_mem_use())

    def _get_mem_use(self):
        return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
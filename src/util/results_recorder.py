import os.path
import sys
assert os.path.exists(".git"), "Script needs to be run from a top-level folder in a git repo."
sys.path.append(os.getcwd())

# Misc
import time
import datetime
import os
import errno
from contextlib import redirect_stdout

from keras.utils import plot_model
from keras.callbacks import CSVLogger, ModelCheckpoint

# Visualise
import matplotlib
matplotlib.use('Agg')  # makes it work without X-server
import matplotlib.pyplot as plt
import json


class ResultsRecorder():
	"""
	Class that is responsible for plotting, recording and saving the results of the run.
	"""
	def __init__(self, results_root, run_name, time_record_filename=""):
		"""
		:param results_root: root folder for saving results from all runs
		"""
		if not os.path.isdir(results_root):
			raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), results_root)

		time_string = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		run_dir_name = time_string + " - " + run_name
		self.save_dir = os.path.join(results_root, run_dir_name)

		os.makedirs(self.save_dir, exist_ok=True)

		self.time_record_filename = time_record_filename

	def __enter__(self):
		self.start_time = datetime.datetime.now()
		return self

	def __exit__(self, exc_type, exc_value, exc_traceback):
		if self.time_record_filename:
			self._record_runtime()
		self.save_dir = ""

	def _record_runtime(self):
		with open(os.path.join(self.save_dir, self.time_record_filename), "w") as fh:
			fh.write("Training complete in: {}".format(datetime.datetime.now() - self.start_time))

	class _Decorators:
		"""
		Inner class containing decorators.
		Declared as class instead of a function because otherwise 'func' is understood as pointer to an object instead.
		More: https://medium.com/@vadimpushtaev/decorator-inside-python-class-1e74d23107f6
		"""
		@staticmethod
		def assert_running(func):
			"""
			Decorator that ensures the results recorder is initialized before a function is called.
			"""
			def wrapper(self, *args, **kwargs):
				assert os.path.isdir(self.save_dir), "Recorder not started."
				return func(self, *args, **kwargs)
			return wrapper

	### General methods ###
	@_Decorators.assert_running
	def record_in_json(self, data, filename):
		"""
		Record any basic python data structure in json
		"""
		assert os.path.isdir(self.save_dir), "Recorder not started."
		with open(os.path.join(self.save_dir, filename), 'w') as fh:
			fh.write(json.dumps(data, indent=4, sort_keys=True))

	### Keras-specific methods ###
	@_Decorators.assert_running
	def save_checkpoint(self, model, filename="checkpoint.h5"):
		"""
		Save keras model.
		"""
		model.save(os.path.join(self.save_dir, filename))

	@_Decorators.assert_running
	def save_best_model_callback(self, filename="best_checkpoint.h5"):
		return ModelCheckpoint(
			os.path.join(self.save_dir, filename),
			save_best_only=True,
			monitor='val_loss',
			mode='min')

	@_Decorators.assert_running
	def plot_simple_model_schema(self, keras_model, filename="model.png"):
		plot_model(keras_model, to_file=os.path.join(self.save_dir, filename), show_shapes=True)

	@_Decorators.assert_running
	def save_summary(self, keras_model, filename="summary.txt"):
		with open(os.path.join(self.save_dir, filename), "w") as fh:
			with redirect_stdout(fh):
				keras_model.summary()

	@_Decorators.assert_running
	def get_csv_logger(self, filename="log.csv"):
		"""
		Create or get the CSVLogger singleton to be passed into Keras as a callback.
		:param filename: name of the output file itself. Path will be prepended automatically.
		:return: CSVLogger instance
		"""
		if not hasattr(self, "csv_logger"):
			self.csv_logger = CSVLogger(os.path.join(self.save_dir, filename), append=True, separator=';')
		return self.csv_logger

	@_Decorators.assert_running
	def save_keras_accuracy_plot(self, history, filename="accuracy.png"):
		"""
		Save historical plots of accuracy for both training and validation sets from keras.
		:return: None
		"""
		plt.plot(history.history['acc'])
		plt.plot(history.history['val_acc'])
		plt.title('Classifier Accuracy')
		plt.ylabel('Accuracy')
		plt.xlabel('Epoch')
		plt.legend(['Training', 'Validation'])
		plt.savefig(os.path.join(self.save_dir, filename))
		plt.gcf().clear()

	@_Decorators.assert_running
	def save_keras_error_plot(self, history, filename="error.png"):
		"""
		Save historical plots of loss for both training and validation sets from keras.
		:return: None
		"""
		plt.plot(history.history['loss'])
		plt.plot(history.history['val_loss'])
		plt.title('Classifier Loss')
		plt.ylabel('Loss')
		plt.xlabel('Epoch')
		plt.legend(['training', 'Validation'])
		plt.savefig(os.path.join(self.save_dir, filename))
		plt.gcf().clear()
